# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 22:10:00 2020

@author: Ondrej Vaculik

Code for calculating remote sensing.
"""

import os
import numpy as np
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from utils import cropper, imghist, imgshow

files = os.listdir(f'./data/cube/SEASONS/')
files = [ f for f in files if f.endswith("N.npy") ]
filters = np.load('./data/cube/SEASONS/filters.npy')
indices = ['ndvi', 'ci-g', 'diff', 'xor']

# Histograms are plotted with 3-sigma range around mean.
def histrange(im):
    (im_mean, im_sigma) = (np.mean(im), np.std(im))
    return (im_mean - 3*im_sigma, im_mean + 3*im_sigma)


def stats(im):
    mi = np.min(im)
    ma = np.max(im)
    me = np.mean(im)
    st = np.std(im)
    cv = np.std(im)/np.mean(im)
    return [mi, ma, me, st, cv]
    

#%% Datacube import and reshape
for date in files:
    name = date[:6]
    data = np.load(f'./data/cube/SEASONS/{date}')
    data = cropper(data, 800, 2000, 0, 2000)
    Path(f'./out/indices/').mkdir(parents=True, exist_ok=True)
    
    #%% Decompose cube to bands with filtername variables
    fnames = ['f'+str(i) for i in filters]
    for i, im in enumerate(data):
        vars()[fnames[i]] = im
    
    #%% NDVI 
    ndvi = (f875-f650)/(f875+f650)
    stats_ndvi = stats(ndvi)
    imgshow(ndvi, f'{name} NDVI', save='png', folder='out\indices\plot')
    imghist(ndvi, f'{name} NDVI', hrange=histrange(ndvi), 
            save='png', folder='out\indices\plot')
    np.save(f'./out/indices/{name}-NDVI', ndvi)
    
    #%% Chorophyll index green
    cig = (f875/f525)-1
    stats_cig = stats(cig)
    imgshow(cig, f'{name} CI-G', save='png', folder='out\indices\plot')
    imghist(cig, f'{name} CI-G', hrange=histrange(cig), 
            save='png', folder='out\indices\plot')
    np.save(f'./out/indices/{name}-CI-G', cig)
    
    #%% Difference
    diff = (f750-f650)/65535
    stats_diff = stats(diff)
    imgshow(diff, f'{name} DIFF 750-650', save='png', folder='out\indices\plot')
    imghist(diff, f'{name} DIFF 750-650', hrange=histrange(diff), 
            save='png', folder='out\indices\plot')
    np.save(f'./out/indices/{name}-DIFF', diff)

    #%% Normalised XOR
    xor = (f650^f750)/np.max(f650^f750)
    stats_xor = stats(xor)
    imgshow(xor, f'{name} XOR 650 750', save='png', folder='out\indices\plot')
    imghist(xor, f'{name} XOR 650 750', hrange=histrange(xor),
            save='png', folder='out\indices\plot')
    np.save(f'./out/indices/{name}-XOR', xor)
    print(f'{name} indices exported.')
    
    #%% Indices statistics export
    df = pd.DataFrame([stats_ndvi,stats_cig, stats_diff, stats_xor], 
                    columns=["Min", "Max", "Mean", "StDev", "CV"], 
                    index=indices);
    Path(f'./out/indices/stats/').mkdir(parents=True, exist_ok=True)
    df.to_csv(f'./out/indices/stats/{name}_indices_stat.csv', encoding='utf-8')
    print(f'{name} indices statistics saved.')
    
    
#%% Boxplot for comparing index values between seasons
for index in indices:
    box_plot_data = []
    dates = []
    plt.figure(dpi=300, figsize=(3.0625,3.0625))
    for date in files:
        data = np.load(f'./data/cube/SEASONS/{date}')
        data = cropper(data, 800, 2000, 0, 2000)
        fnames = ['f'+str(i) for i in filters]
        for i, im in enumerate(data):
            vars()[fnames[i]] = im
            
        if index == "ndvi":
            idx = (f875-f650)/(f875+f650)
        elif index == "ci-g":
            idx = (f875/f525)-1
        elif index == "diff":
            idx = (f750-f650)/65535
        elif index == "xor":
            idx = (f650^f750)/np.max(f650^f750)
            
        dates.append(date[:6])
        box_plot_data.append(idx.flatten())
 
    plt.boxplot(box_plot_data, labels=dates,showmeans=True, meanline=True, 
                showfliers=False, medianprops=dict(color='tab:blue'), 
                meanprops=dict(color='tab:red'))
    plt.xlabel('Date [YYMMDD]')
    plt.ylabel(f'{index.upper()} [-]')
    plt.savefig(f'./out/indices/stats/boxplot_{index}.pdf', bbox_inches='tight')
    plt.show()
    
#%% Plot selected crop of individual indices and save it    
files = os.listdir('./out/indices/')
files = [ f for f in files if f.endswith(".npy") ]
def index_crop(files, idx):
    files = [ f for f in files if f.endswith(f"{idx}.npy") ]
    for file in files:
        idx = np.load(f'./out/indices/{file}')[0:500,0:500]
        imgshow(idx, name=file[:-4], save='png', folder='out/indices/crop')
    
    
index_crop(files, 'NDVI')
index_crop(files, 'CI-G')