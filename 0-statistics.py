# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 10:31:48 2020

@author: Ondrej Vaculik

Computes statistic info of the fits set/datacube
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from skimage.exposure import histogram


def stats(data, Xwhite, Ywhite, filters, name, save=False):
    bands,x,y = np.shape(data)
    mins, maxes, means, stdevs, cv, snr, whitemeans = [ [] for _ in range(7)]
    for im in data:
        mins.append(np.min(im))
        maxes.append(np.max(im))
        means.append(np.mean(im))
        stdevs.append(np.std(im))
        cv.append(np.std(im)/np.mean(im))
        snr.append(10*np.log10(np.mean(im[Xwhite, Ywhite])/
                               np.std(im[Xwhite, Ywhite])))
        whitemeans.append(np.mean(im[Xwhite, Ywhite]))
    df = pd.DataFrame([mins, maxes, means, stdevs, cv, snr, whitemeans], 
                   index=["Min", "Max", "Mean", "StDev", "CV", 
                          "SNR [dB]", "White Mean"], 
                   columns=filters);
    if save==True:
        df.to_csv(f'./data/stat/{name[:-6]}_stat.csv', encoding='utf-8')
        print(f'{name} statistics saved.')
    print(f'Stats of {name}')
    with pd.option_context('display.max_rows', None, 
                           'display.max_columns', df.shape[1]):
        print(df.round(2),'\n')
        
        
def hists(data, minval, maxval, bands, save=False):
    plt.figure(dpi=300)
    plt.title(f'{files[idx][0:6]}')
    for i in range(0,6):
        h, hc = histogram(data[i])
        plt.plot(hc, h, alpha=0.75)
        plt.xlim(minval,maxval)
        plt.legend(bands)
    name = str(files[idx])[0:6]
    if save==True:
        plt.savefig(f'./data/stat/{name}_hist.png')
        # plt.savefig(f'./data/stat/{name}_hist.pdf')
    plt.show()

#%% Parameters        
normareas = np.load('./data/cube/SEASONS/normareas.npy')
filters = np.load('./data/cube/SEASONS/filters.npy')

#%% Read fits file location
files = os.listdir('./data/cube/SEASONS')
files = [ f for f in files if f.endswith("N.npy") ]
print(f'Folder contains {len(files)} files:', files, '\n')

for idx, file in enumerate(files):
    cube = np.load(f'./data/cube/SEASONS/{file}')
    Xnorm = slice(normareas[idx][0][0], normareas[idx][0][1])
    Ynorm = slice(normareas[idx][1][0], normareas[idx][1][1])
    
    stats(cube, Xnorm, Ynorm, filters=filters, name=f'{file}', save=True)
    hists(cube, 14000, 30000, filters, save=True)

#%% Histogram
