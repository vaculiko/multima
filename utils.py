# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 22:55:28 2020

@author: Ondrej Vaculik

Utility package for multima
"""
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

def cropper(cube, x1, x2, y1, y2):
    """
    Crops the spectral cube by defined slicing.
    
    Parameters
    ----------
    cube : Numpy array
        Input multispecral datacube.
    x1 : int
        X axis slice beginning.
    x2 : int
        X axis slice end.
    y1 : int
        Y axis slice beginning.
    y2 : int
        Y axis slice end.

    Returns
    -------
    Numpy array
        Same as input, but cropped.

    """
    cropped = []
    for im in cube:
        cropped.append(im[x1:x2,y1:y2])
    return np.asarray(cropped)


def reshaper(cube):
    """
    Reshape cube to spectral vectors.

    Parameters
    ----------
    cube : Numpy array
        Stacked images for individual spectral bands.
        The shape is ('bands', 'x', 'y').

    Returns
    -------
    Numpy array
        Array of spectral vectors, input for classification.

    """
    [layers, dimx, dimy] = np.shape(cube)
    pixels = []
    for xpix in range(0, dimx):
        for ypix in range(0, dimy):
            pixels.append(cube[:,xpix,ypix])
    return np.asarray(pixels)


def imgshow(im, name='', cmap='nipy_spectral', axis=False, save=False, folder='out'):
    """
    Handy image plotting and saving function.
    
    Parameters
    ----------
    im : Array
        Input image.
    name : str, optional
        Name of the image used for title and save name. The default is ''.
    cmap : str, optional
        Colormap from Matplotlib. The default is 'nipy_spectral'.
    axis : bool, optional
        Enables the plot axis.
    save : False/'pdf'/'png', optional
        Toggle 'pdf' or 'png' saving to './out/{name}.{save}' direcotry. 
        The default is False.
    folder : Name of saving folder
    
    Returns
    -------
    Shows image.

    """
    plt.figure(dpi=300)
    plt.title(name)
    plt.imshow(im, cmap=cmap)
    plt.colorbar()
    if axis==False:
        plt.axis('off')
    if save!=False:
        Path(f'./{folder}').mkdir(parents=True, exist_ok=True)
        plt.savefig(f'./{folder}/{name}.{save}', bbox_inches='tight')
    plt.show()


def imghist(im, name='', hrange=(0,1), save=False, folder='out'):
    plt.figure(dpi=300)
    plt.title(name)
    plt.hist(im.ravel(), bins=512, range=hrange, fc='white', ec='gray')
    plt.xlabel('Pixel value [-]')
    plt.ylabel('Number of pixels [-]')
    if save!=False:
        Path(f'./{folder}').mkdir(parents=True, exist_ok=True)
        plt.savefig(f'./{folder}/hist_{name}.{save}', bbox_inches='tight')
    plt.show()
      

def featureplot(cube, z1, z2, filters, name='', save=False, folder='out'):
    """
    2D feature plotting.

    Parameters
    ----------
    cube : Numpy array
        Stacked images for individual spectral bands.
        The shape is ('bands', 'x', 'y').
    z1 : int
        First selected feature (filter position).
    z2 : int
        Second selected feature (filter position).
    filters : list
        List of filter central wavelengths, used for axis labels.
    name : str, optional
        Name of the image used for title and save name. The default is ''.
    save : False/'pdf'/'png', optional
        Toggle 'pdf' or 'png' saving to 
        './out/fetaureplot_{name}.{save}' direcotry. 
        The default is False.

    Returns
    -------
    2D feature plot.

    """
    [layers, dimx, dimy] = np.shape(cube)
    points = [np.reshape(cube[z1], dimx*dimy,1),
              np.reshape(cube[z2], dimx*dimy,1)]
    
    fig = plt.figure(dpi=300)
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    plt.title(name)
    plt.scatter(points[0], points[1], s=0.5, marker='.')
    plt.xlabel(str(filters[z1])+'nm')
    plt.ylabel(str(filters[z2])+'nm')
    if save!=False:
        Path(f'./{folder}').mkdir(parents=True, exist_ok=True)
        plt.savefig(f'./{folder}/feature_{name}.{save}', bbox_inches='tight')
    plt.show()


def labelplot(im, title, cbpos='vertical', save=False, folder='out'):
    # shows labeled image with nice colorbar
    plt.figure(dpi=300)
    colors = plt.get_cmap('gist_rainbow', np.max(im)-np.min(im)+1)
    img = plt.imshow(im, cmap=colors, vmin=np.min(im)-.5, vmax=np.max(im)+.5)
    plt.colorbar(img, orientation=cbpos, 
                 ticks=np.arange(np.min(im), np.max(im)+1))
    plt.axis('off')
    plt.title(title)
    if save!=False:
        Path(f'./{folder}').mkdir(parents=True, exist_ok=True)
        plt.savefig(f'./{folder}/labels_{title}.{save}', bbox_inches='tight')
    plt.show()
    