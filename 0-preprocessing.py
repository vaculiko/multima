# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 10:31:48 2020

@author: Ondrej Vaculik

Converts individual FITS files to one normalised datacube and exports as npy
"""

import os
import numpy as np
from pathlib import Path
from astropy.io import fits

# Select input data and parameters
name = 'CMYK' # CMYK or SEASONS
(newmin, newmax) = (16384, 49152)

if name == 'CMYK':
    dates = [171211] # for CMYK
    normareas = [[[1500, 1520], [300, 330]]] # for CMYK
    Path(f'./data/cube/CMYK').mkdir(parents=True, exist_ok=True)
    np.save('./data/cube/CMYK/normareas.npy',normareas)
else:
    dates = [190228, 190404, 190604, 191030]
    normareas = [[[1540, 1560], [1535, 1565]],
                  [[1445, 1455], [600, 660]],
                  [[1830, 1850], [875, 905]],
                  [[1535, 1555], [830, 860]]]
    Path(f'./data/cube/SEASONS').mkdir(parents=True, exist_ok=True)
    np.save('./data/cube/SEASONS/normareas.npy', normareas)

#%% Load images
cubes = []
for date in dates:
    # Read fits file location
    files = os.listdir(f'./data/fits/{date}/')
    files = [ f for f in files if f.endswith(".fits") ]
    print(f'Folder {date} contains {len(files)} files.')
    
    # pulls filter central wavelength from filename
    filters = [ int(f[8:12]) for f in files ] 
    print(f'Datacube {date} composed of {len(filters)} filters:', filters, 'nm')

    # import and horizontal flipping of images
    cube = np.asarray([np.flip(fits.getdata('./data/fits/'+str(date)+'/'+f), 0) 
                       for f in files]) 
    cubes.append(cube)
    
if name == 'CMYK':
    np.save('./data/cube/CMYK/filters.npy',filters)
else:
    np.save('./data/cube/SEASONS/filters.npy',filters)

#%% Normalisation procedure
means = []
cubesN = []
for i, cube in enumerate(cubes):
    cubeNi = []
    Xnorm = slice(normareas[i][0][0], normareas[i][0][1])
    Ynorm = slice(normareas[i][1][0], normareas[i][1][1])
    for im in cube:
        imin, imax = (np.min(im), np.max(im[Xnorm, Ynorm]))
        im = (im - imin)*((newmax - newmin)/(imax - imin)) + newmin
        cubeNi.append(im)
        means.append(np.mean(im[Xnorm, Ynorm]))
    print(f'Cube {dates[i]} mean calculated.')
    cubesN.append(cubeNi)
    
mainmean = np.mean(means)
    
meansH = []
for i, cubeN in enumerate(cubesN):
    cubeH = []
    Xnorm = slice(normareas[i][0][0], normareas[i][0][1])
    Ynorm = slice(normareas[i][1][0], normareas[i][1][1])
    for imN in cubeN:
        imN += (mainmean - np.mean(imN[Xnorm, Ynorm]) )  
        cubeH.append(np.around(imN))
        meansH.append(np.mean(imN[Xnorm, Ynorm]))
    cubeOUT = np.ndarray.astype(np.asarray(cubeH), int)
    cubeOUT[cubeOUT > 65535] = 65535 # fix clipping values
    if name=='CMYK':
        np.save(f'./data/cube/CMYK/{dates[i]}_N', cubeOUT)
    else:
        np.save(f'./data/cube/SEASONS/{dates[i]}_N', cubeOUT)
    print(f'Cube {dates[i]} exported.')
