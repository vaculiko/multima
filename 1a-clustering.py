# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 10:31:48 2020

@author: Ondrej Vaculik

Computes clusters of pixels using K-Means
"""

import os
import time
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from sklearn.cluster import MiniBatchKMeans
from utils import cropper, reshaper, labelplot


def main():   
    dataset = 'CMYK' 
    data, filters, dim = cmyk_import()
    k = elbow(data, 10, dataset, dataset)
    # k=6
    labels, centers = kmeans(data, k, dim, dataset, dataset)
    plot_avgspec(data, k, labels, dataset, filters, dataset, clusters=True)
    
    name = 'CMYK_AE'
    latent = np.load('./out/ae/encoded_CMYK.npy')
    k = elbow(latent, 10, dataset, name)
    labels, centers = kmeans(latent, k, dim, dataset, name, 
                              remap=[0,3,2,1,5,4])
    plot_latent(latent, k, labels, centers, dataset, name)
    
    # kk=[6,8,8,4] # selected k values for SEASONS
    # i=0
    dataset = 'SEASONS'
    files = os.listdir('./data/cube/SEASONS')
    files = [ f for f in files if f.endswith("N.npy") ]
    for f in files:
        name = f[:6]
        data, filters, dim = season_import(f)
        k = elbow(data, 10, dataset, name)
        # k=kk[i]
        labels, centers = kmeans(data, k, dim, dataset, name)
        plot_avgspec(data, k, labels, dataset, filters, name, clusters=True)
        # i+=1
        
    files = os.listdir('./out/ae')
    files = [ f for f in files if f.endswith("N.npy") ]
    for f in files:
        name = f'{f[8:14]}_AE'
        latent = np.load(f'./out/ae/{f}')
        k = elbow(latent, 10, dataset, name)
        labels, centers = kmeans(latent, k, dim, dataset, name)
        plot_latent(latent, k, labels, centers, dataset, name)
        
    label_crop(crop=500)
    label_crop(crop=500, ae=True)


def cmyk_import(file='./data/cube/CMYK/filters.npy'):
    Path(f'./out/clustering/CMYK').mkdir(parents=True, exist_ok=True)
    filters = np.load(file)
    file = np.load('./data/cube/CMYK/171211_N.npy')
    crop = cropper(file, 1250, 1900, 300, 1100)
    bands, dimx, dimy = np.shape(crop)
    dim = [dimx, dimy]
    data = reshaper(crop)
    print('Dataset: CMYK', 'Datatype:', type(data),' Shape:', np.shape(data))
    return data, filters, dim


def season_import(file):
    Path(f'./out/clustering/SEASONS').mkdir(parents=True, exist_ok=True)
    filters = np.load('./data/cube/SEASONS/filters.npy')
    season = np.load(f'./data/cube/SEASONS/{file}')
    crop = cropper(season, 800, 2000, 0, 2000)
    bands, dimx, dimy = np.shape(crop)
    dim = [dimx, dimy]
    data = reshaper(crop)
    return data, filters, dim


#%% Elbow analysis using minibatch K-Means
def elbow(data, kmax, dataset, name):
    inertia = []
    k_clusters = range (1,kmax+1)
    for n in k_clusters:
        mbk = MiniBatchKMeans(n, verbose=0, random_state=42).fit(data)
        inertia.append(mbk.inertia_)
        print(f'Elbow analysis for {n} clusters solved.')
    
    # Elbow analysis plot
    plt.figure(dpi=300)
    plt.plot(k_clusters, inertia, 'o-', color='tab:blue')
    # plt.yscale('log')
    plt.xticks(k_clusters)
    plt.grid()
    plt.xlabel('Clusters [-]')
    plt.ylabel('Distortion [-]')
    plt.title(f'{name} Elbow analysis')
    plt.savefig(f'./out/clustering/{dataset}/{name}_elbow.pdf', 
                bbox_inches='tight')
    plt.show()
    
    while True:
        try:
            print(f'\n Dataset {name}')
            k = int(input('Set value for k='))
            if k in range(kmax+1):
                break
        except:    
            pass
        print('\nIncorrect input, try again')
    return k


#%% Minibatch K-Means
def kmeans(data, k, dim, dataset, name, remap=False):
    mbk = MiniBatchKMeans(k, verbose=1, random_state=42).fit(data)
    mbkl = mbk.labels_
    mbkc = mbk.cluster_centers_[mbk.predict(data)]
    mbkout = mbkl.reshape(dim[0], dim[1])
    if remap!=False:
        mbkout = cluster_remap(mbkout, remap)
    
    labelplot(mbkout, title=f'{name} k-means {k} clusters', 
               folder = f'out/clustering/{dataset}', save='png')
    np.save(f'./out/clustering/clustering_{name}', mbkout)
    return mbkl, mbkc


#%% Plot latent space with labels
def plot_latent(latent, k, mbkl, mbkc, dataset, name):
    plt.figure(dpi=300)
    plt.title(f'Autoencoder {name} Latent Space k-means')
    latent = np.transpose(latent)
    plt.scatter(latent[0], latent[1], c=mbkl, marker='.', 
                vmin=-0.5, vmax=k-0.5,
                cmap=plt.get_cmap('gist_rainbow', k))
    plt.colorbar(ticks=np.arange(0, k))
    plt.scatter(mbkc[:, 0], mbkc[:, 1], c='k', marker='+')
    plt.xlabel('$c_1$ [-]')
    plt.ylabel('$c_2$ [-]')
    plt.savefig(f'./out/clustering/{dataset}/{name}_latent_km.png', 
                bbox_inches='tight')
    plt.show()
    print(f'File {name} analysed and plots exported.\n')
        

#%% Average spectra for each label
def plot_avgspec(data, k, mbkl, dataset, filters, name, clusters=False):
    specs = [[mbkl[i], s/65536] for i,s in enumerate(data)]
    avgspec = []
    classspec = []
    for n in range(0, k):
        nspec = []
        for s in specs:    
            if s[0]==n:
                nspec.append(s[1])
        classspec.append(nspec)
        avgspec.append(np.mean(nspec, axis=0).reshape(len(filters)))
        
    # Average spectra plot - all bands
    plt.figure(dpi=300)
    plt.rcParams["axes.prop_cycle"] = plt.cycler(
        "color", plt.cm.gist_rainbow(np.linspace(0,1,k)))
    for n in range(0, k):
        plt.plot(filters, avgspec[n], marker='o', alpha=0.75)
        # plt.ylim(0.2 ,1)
    plt.title(f'{name} Average spectra ')
    plt.legend(range(0, k))
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Relative pixel value [-]')
    plt.savefig(f'./out/clustering/{dataset}/{name}_avgspec.pdf', 
                bbox_inches='tight')
    plt.show()
        
    # Combined average spectra box plot
    def boxplot(axrow, x, y):
         axrow[0].boxplot(x,labels=filters, showmeans=True, meanline=True,
                    showfliers=False, medianprops=dict(color='tab:blue'), 
                    meanprops=dict(color='tab:red'))
         axrow[0].set_title(f'Class {n}')
    
         axrow[1].boxplot(y,labels=filters, showmeans=True, meanline=True,
                    showfliers=False, medianprops=dict(color='tab:blue'), 
                    meanprops=dict(color='tab:red'))
         axrow[1].set_title(f'Class {n+1}')
    
    nrows = int(np.ceil(k/2))
    n = 0
    fig, axes = plt.subplots(nrows, 2, figsize=(6.125,8.75), dpi=300, sharex=True)
    fig.suptitle(f'{name} Average spectra for classes', size=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.93)
    
    for row in axes:
        x = np.array(classspec[n])
        if n+1<k:
            y = np.array(classspec[n+1])
        else:
            # repeats class 0 to plot odd number of classes sucessfully
            y = np.array(classspec[0])
        boxplot(row, x, y)
        n = n+2
    
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axis
    plt.tick_params(labelcolor='none', top=False, bottom=False, 
                    left=False, right=False)
    plt.grid(False)
    plt.xlabel('Wavelength [nm]')
    plt.ylabel('Relative pixel value [-]')
    plt.savefig(f'./out/clustering/{dataset}/{name}_avgbox.pdf', 
                bbox_inches='tight')
    plt.show()
    # Boxplot per cluster
    if clusters == True:
        for n in range(0, k):
            plt.figure(dpi=300, figsize=(3.0625,3.0625))
            plt.boxplot(np.array(classspec[n]),labels=filters, 
                        showmeans=True, meanline=True, showfliers=False,
                        medianprops=dict(color='tab:blue'), 
                        meanprops=dict(color='tab:red'))
            # plt.ylim(0,1)
            plt.title(f'{name} - Class {n}')
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Relative pixel value [-]')
            Path(f'./out/clustering/{dataset}/{name}').mkdir(parents=True, exist_ok=True)
            plt.savefig(f'./out/clustering/{dataset}/{name}/{name}_cluster_{n}_avgspec.pdf', 
                        bbox_inches='tight')
            plt.show()
    print(f'File {name} analysed and plots exported.\n')


#%% K-means output cropper
def label_crop(crop=500, ae=False):
    if ae==True:
        kmae = os.listdir('./out/clustering/')
        kmae = [ f for f in kmae if f.endswith("AE.npy") ]
        for i,k in enumerate(kmae[:-1], 0):
            im = np.load(f'./out/clustering/{k}')
            labelplot(im[0:crop,0:crop], kmae[i][11:20], 
                      save='png', folder='./out/clustering/SEASONS/crop')
    else:    
        km = os.listdir('./out/clustering/')
        km = [ f for f in km if not f.endswith("AE.npy") ]
        for i,k in enumerate(km[:-3], 0):
            im = np.load(f'./out/clustering/{k}')
            labelplot(im[0:crop,0:crop], km[i][11:17],
                      save='png', folder='./out/clustering/SEASONS/crop')
      
#%% K-means label index remapper     
def cluster_remap(kmout, remap):
    k = np.max(kmout)+1
    if len(remap) != k:
        print('Wrong number of labels!')
    else:
        for i in range(k):
            kmout[kmout==i] = (100+i)
        for old,new in enumerate(remap, 100):
            kmout[kmout==old] = new
        return kmout


  #%%      
if __name__ == "__main__":
    start_time = time.time()
    main()
    print("Time elapsed: %.2f seconds " % (time.time() - start_time)) 
