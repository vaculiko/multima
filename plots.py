# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 11:15:44 2020

@author: Ondrej Vaculik

Plotting for Masters thesis
"""

import os
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets.samples_generator import make_blobs
Path(f'./plots').mkdir(parents=True, exist_ok=True)

#%% File import
files = os.listdir('./data/usgs')
files = [ f for f in files if f.endswith(".txt") ]
print('Located files: ', files)

concrete = {'resolution': np.genfromtxt(f'./data/usgs/{files[0]}', skip_header=1),
             'microns': np.genfromtxt(f'./data/usgs/{files[1]}', skip_header=1),
             'data': np.genfromtxt(f'./data/usgs/{files[2]}', skip_header=1)}
             
grass_dry = {'resolution': np.genfromtxt(f'./data/usgs/{files[3]}', skip_header=1),
             'microns': np.genfromtxt(f'./data/usgs/{files[4]}', skip_header=1),
             'data': np.genfromtxt(f'./data/usgs/{files[5]}', skip_header=1)}

grass_wet = {'resolution': np.genfromtxt(f'./data/usgs/{files[6]}', skip_header=1),
             'microns': np.genfromtxt(f'./data/usgs/{files[7]}', skip_header=1),
             'data': np.genfromtxt(f'./data/usgs/{files[8]}', skip_header=1)}

cha = np.genfromtxt(f'./data/usgs/{files[9]}').transpose()

chb = np.genfromtxt(f'./data/usgs/{files[10]}').transpose()

#%% Filter windows
x = np.arange(0,1.11,0.0001)
f =  [np.zeros(4310),
     np.ones(100),
     np.zeros(790),
     np.ones(100),
     np.zeros(1150),
     np.ones(100),
     np.zeros(900),
     np.ones(100),
     np.zeros(1140),
     np.ones(120),
     np.zeros(1090),
     np.ones(200),
     np.zeros(1000)]      
ff = [y for x in f for y in x]  

#%% Grass spectral reflectance plot (concrete not used)
plt.figure(dpi=300)
plt.plot(grass_wet['microns'], grass_wet['data'], color='green')
plt.plot(grass_dry['microns'], grass_dry['data'], color='orange')
# plt.plot(concrete['microns'], concrete['data'], color='black')
plt.fill_between(x,ff, alpha=0.5, step='post', facecolor='gray')

plt.xlim(0.3, 1.1)
plt.ylim(0, 0.8)
plt.legend(['Fresh grass','Dry grass','Filters']) #'Concrete',
plt.xlabel('Wavelength [$\mathrm{\mu}$m]')
plt.ylabel('Reflectance [-]')

plt.savefig('./plots/spec-grass.pdf', bbox_inches='tight')
plt.show()

#%% Chlorophyll dissolved in diethyl ether plot
plt.figure(dpi=300)
plt.plot(cha[0]/1000, cha[1]/np.max(cha[1]))
plt.plot(chb[0]/1000, chb[1]/np.max(chb[1]), color='red')
plt.fill_between(x, ff, alpha=0.5, step='post', facecolor='gray')

plt.xlim(0.3, 0.8)
plt.ylim(0, 1)
plt.legend(['CH-a','CH-b','Filters'])
plt.xlabel('Wavelength [$\mathrm{\mu}$m]')
plt.ylabel('Absorption [-]')

plt.savefig('./plots/spec-ch-ab+filters.pdf', bbox_inches='tight')
plt.show()

#%% Solar radiance spectra
astm = np.genfromtxt('./data/solar/solar-spectrum-ASTMG173.txt', skip_header=2)
astm = np.transpose(astm)
solar = {'microns': astm[0]/1000, 'etr': astm[1], 
         'tilt': astm[2], 'direct': astm[3]}

h = 6.6260693e-34
c = 299792458.0
k = 1.380658e-23

def irad(lam, T):
    # returns solar irradiation for input wavelength and temperature
    p = (2.0*h*c**2)/(lam**5)
    e = h*c/(lam*k*T)
    return (np.pi*p*2.177e-5)/(np.exp(e) - 1)

wavelengths = np.arange(2e-7, 1.2e-6, 1e-9)
sun = irad(wavelengths, 5780.)

plt.figure(dpi=300)
plt.plot(solar['microns'], solar['direct'])
plt.plot(wavelengths*1e6, sun/1e9)
# plt.fill_between(x, ff, alpha=0.5, step='post', facecolor='gray')

plt.xlim(0.2,1.2)
plt.xticks(np.arange(0.2,1.21,0.1))

plt.legend(['Data','Model','Filters'])
plt.xlabel('Wavelength [$\mathrm{\mu m}$]')
plt.ylabel('Spectral irradiance [$\mathrm{W}\cdot \mathrm{m}^{-2}\cdot \mathrm{nm}^{-1}$]')

plt.savefig('./plots/sun-blackbody-irradiance.pdf', bbox_inches='tight')
plt.show()

#%% K-means example
X, y_true = make_blobs(n_samples=300, centers=4,
                       cluster_std=0.60, random_state=0)
plt.figure(figsize=(3,3), dpi=300)
plt.xlabel('$x_1$ [-]')
plt.ylabel('$x_2$ [-]')
plt.scatter(X[:, 0], X[:, 1], marker='.', s=30);
plt.savefig('./plots/km-blobs.pdf', bbox_inches='tight')
plt.show()

kmeans = KMeans(n_clusters=4)
kmeans.fit(X)
y_kmeans = kmeans.predict(X)
plt.figure(figsize=(3,3), dpi=300)
plt.xlabel('$x_1$ [-]')
plt.ylabel('$x_2$ [-]')
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, marker='.', s=30, cmap='Set1')
centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, 
            alpha=0.75, marker='+');
plt.savefig('./plots/km-clusters.pdf', bbox_inches='tight')
plt.show()

#%% K-means elbow plot
inertia = []
n_clusters = range (1,8)
for n in n_clusters:
    km = KMeans(n, max_iter=10, 
                          verbose=0, random_state=42).fit(X)
    inertia.append(km.inertia_)
    print(f'K-means for {n} clusters solved.')

# Elbow analysis plot
plt.figure(dpi=300)
plt.plot(n_clusters, inertia, 'o-')
plt.xticks(n_clusters)
plt.grid()
plt.xlabel('Clusters [-]')
plt.ylabel('Inertia [-]')
plt.title('Elbow analysis')
plt.savefig('./plots/km-elbow.pdf', bbox_inches='tight')
plt.show()
