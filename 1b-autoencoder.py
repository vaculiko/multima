# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 20:37:24 2020

@author: Ondrej Vaculik

Neural network trained to compress multispectral data using autoencoder
"""

import os
import time
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
from utils import reshaper, cropper
from keras.layers import Input, Dense
from keras.models import Model
from tensorflow import random
from sklearn.model_selection import train_test_split
# Disable TF build errors
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# Fix seed to get reproducible results
np.random.seed(42)
random.set_seed(42)


def main():
    file = np.load('./data/cube/CMYK/171211_N.npy')
    crop = [1250, 1900, 300, 1100]
    ae('CMYK', file, crop, 20, 1024, latent=True)
    
    files = os.listdir('./data/cube/SEASONS')
    files = [ f for f in files if f.endswith("N.npy") ]
    for f in files:
        file = np.load(f'./data/cube/SEASONS/{f}')
        crop = [800, 2000, 0, 2000]
        ae(f[:8], file, crop, 10, 2048, latent=True)
    
    
def ae(name, file, crop, epochs, batch_size, deep=False, latent=False):
    data = reshaper(cropper(file, crop[0], crop[1], crop[2], crop[3]))/65535
    x_train, x_test = train_test_split(data, test_size=0.2, random_state=24)
    x_train = np.array(x_train)
    x_test = np.array(x_test)
    print(f"Dataset {name} imported and prepared for training.")

    #%% Autoencoder for dimensionality reduction
    encoding_dim = 2
    # Input spectrum data placeholder
    input_spec = Input(shape=(6,))
    # Encoding layer
    encoded = Dense(encoding_dim, activation='sigmoid')(input_spec)
    # Decoding layer
    decoded = Dense(6, activation='sigmoid')(encoded)
    # AE initialization
    autoencoder = Model(input_spec, decoded)
    encoder = Model(input_spec, encoded)
    # Create a placeholder for an encoded (2-dimensional) input
    encoded_input = Input(shape=(encoding_dim,))
    # Retrieve the last layer of the autoencoder model
    decoder_layer = autoencoder.layers[-1]
    # Create the decoder model
    decoder = Model(encoded_input, decoder_layer(encoded_input)) 
    autoencoder.compile(optimizer='SGD', loss='mean_squared_error')
    print("Autoencoder compiled.")

    # Begin training
    ae = autoencoder.fit(x_train, x_train,
                    epochs=epochs,
                    batch_size=batch_size, # should be in powers of 2
                    shuffle=True,
                    validation_data=(x_test, x_test))
    print("Training ended.")

    #%% AE model plots
    plt.figure(dpi=300)
    plt.plot(ae.history['loss'])
    plt.plot(ae.history['val_loss'])
    plt.grid()
    step = 2 if epochs <= 10 else 4
    plt.xticks(np.arange(0, epochs+1, step))
    plt.title(f'{name} $-$ SGD + MSE, batch {batch_size}')
    plt.ylabel('Loss [-]')
    plt.xlabel('Epoch [-]')
    plt.legend(['Train', 'Test'], loc='upper right')
    plt.savefig(f'./out/ae/model/ae_train_{name}.pdf')
    plt.show()

    #%% Encoded data and model saving
    Path(f'./out/ae/model').mkdir(parents=True, exist_ok=True)
    encoded_spec = encoder.predict(data)
    np.save(f'./out/ae/encoded_{name}', encoded_spec)
    encoder.save(f'./out/ae/model/encoder_{name}.h5')
    decoder.save(f'./out/ae/model/decoder_{name}.h5')
    autoencoder.save(f'./out/ae/model/autoencoder_{name}.h5')
    print("Encoded data and model saved.\n") 

    #%% 2D latent space plot
    if latent==True:
        latent = np.transpose(encoded_spec)
        plt.figure(dpi=300)
        plt.title(f'Autoencoder {name} Latent Space')
        plt.scatter(latent[0], latent[1], s=1, marker=',')
        plt.xlabel('$c_1$ [-]')
        plt.ylabel('$c_2$ [-]')
        plt.savefig(f'./out/ae/model/ae_latent_{name}.png')
        plt.show()


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("Time elapsed: %.2f seconds " % (time.time() - start_time)) 
