# Multispectral Image Analysis

Code for multispectral image analysis for my Master's Thesis - Multispectral image analysis using artificial neural networks. Thesis text available [here](https://theses.cz/id/dwi8mf/DP-Vaculik-OPT-2020.pdf).

## Processing Scripts

`0-preprocessing.py` - FITS image import and normalization  
`0-statistics.py` - Normalized dataset inspection tool for normalization evaluation  
`1a-clustering.py` - *k*-means clustering implementation, combined with elbow analysis and plotted output  
`1b-autoencoder.py` - Autoencoder training and data compression script  
`indices.py` - Calculates defined GIS indices, outputs plots and histograms  
`plots.py` - Utility for plotting data used in the thesis text  
`utils.py` - Custom functions for data manipulation and easy plotting

## Input Data

FITS images, obtained by CCD camera combined with a set of narrow-band spectral filters.